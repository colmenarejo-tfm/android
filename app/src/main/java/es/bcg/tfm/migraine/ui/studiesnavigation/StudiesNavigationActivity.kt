package es.bcg.tfm.migraine.ui.studiesnavigation

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import es.bcg.tfm.migraine.R
import es.bcg.tfm.migraine.ui.studiesnavigation.fragmet.patients.PatientsFragment
import es.bcg.tfm.migraine.ui.studiesnavigation.fragmet.summary.SummaryFragmet

class StudiesNavigationActivity : AppCompatActivity() {

    private var fragments = ArrayList<Fragment>(2)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(es.bcg.tfm.migraine.R.layout.activity_studies_info)

        val bottomNavigation: BottomNavigationView = findViewById(es.bcg.tfm.migraine.R.id.navigationView)

        bottomNavigation.setOnNavigationItemSelectedListener(
            object : BottomNavigationView.OnNavigationItemSelectedListener {
                override fun onNavigationItemSelected(item: MenuItem): Boolean {
                    when (item.itemId) {
                        R.id.studies_navigation_summary -> {
                            switchFragment(0)
                            return true
                        }
                        R.id.studies_navigation_patients -> {
                            switchFragment(1)
                            return true
                        }
                    }
                    return false
                }
            })

        fragments.add(SummaryFragmet())
        fragments.add(PatientsFragment())

        switchFragment(0)
    }

    private fun switchFragment(pos: Int) {
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.frame_patient_navigation, fragments[pos])
            .commit()
    }

}