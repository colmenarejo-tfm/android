package es.bcg.tfm.migraine.ui.patientnavigation.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import es.bcg.tfm.migraine.R

class MigraineSummaryFragment : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.activity_patient_info, container, false)
    }

    companion object {
        fun newInstance(): MigraineSummaryFragment {
            return MigraineSummaryFragment()
        }
    }

}