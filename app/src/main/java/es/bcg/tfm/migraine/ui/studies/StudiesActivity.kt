package es.bcg.tfm.migraine.ui.studies

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.widget.GridLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import es.bcg.tfm.migraine.R
import es.bcg.tfm.migraine.ui.studies.viewmodel.StudyViewModel
import es.bcg.tfm.migraine.ui.studiesnavigation.StudiesNavigationActivity


class StudiesActivity : AppCompatActivity() {

    lateinit var gridLayout: GridLayout

    lateinit var dm: DisplayMetrics

    var imagesize: Double = 0.toDouble()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_studies)

        gridLayout = findViewById<View>(R.id.studies) as GridLayout
        dm = resources.displayMetrics
        imagesize = dm.widthPixels * 0.20


        var studies : MutableList<StudyViewModel> = mutableListOf()
        studies.add(StudyViewModel(1, "HUP (2018)", "2018/10/01"))
        studies.add(StudyViewModel(2, "Sanitas (2018)", "2018/05/17"))

        gridLayout.post { setGridValues(studies) }
    }


    private fun setGridValues(studies : List<StudyViewModel>) {
        val gWidth = gridLayout.width
        val w = gWidth / 2
        val h = (w * 0.80).toInt()

        val inflater = getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        for (study in studies) {
            val params = GridLayout.LayoutParams()
            params.width = w
            params.height = h

            val child = inflater.inflate(R.layout.item_study, null)

            //setting background color
//            val backgroundcolor = child.findViewById(R.id.cell_background) as RelativeLayout
//            backgroundcolor.setBackgroundResource(bg_color[i])

            //setting image
//            val img_icon = child.findViewById(R.id.cell_image) as ImageView
            //setting size of icon based on screen size
//            val iparams = img_icon.layoutParams as RelativeLayout.LayoutParams
//            iparams.width = imagesize.toInt()
//            iparams.height = imagesize.toInt()
//            img_icon.layoutParams = iparams
//            img_icon.setImageResource(img[i])


            //setting title
            val tvTitle = child.findViewById(R.id.cell_text) as TextView
            tvTitle.text = study.name

            val tvTitle2 = child.findViewById(R.id.cell_text_2) as TextView
            tvTitle2.text = study.startDate

            child.setOnClickListener(View.OnClickListener {
                val intent = Intent(baseContext, StudiesNavigationActivity::class.java)
                var bundle = Bundle()
                bundle.putParcelable("study", study)
                intent.putExtra("bundleStudy",bundle)
                startActivity(intent)
            })
            gridLayout.addView(child, params)
        }
    }

}
