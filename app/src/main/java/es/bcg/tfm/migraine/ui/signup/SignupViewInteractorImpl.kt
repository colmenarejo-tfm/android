package es.bcg.tfm.migraine.ui.signup

import es.bcg.tfm.migraine.domain.core.exception.FailureType
import es.bcg.tfm.migraine.domain.model.user.SignupModel
import es.bcg.tfm.migraine.domain.model.user.UserModel
import es.bcg.tfm.migraine.presentation.features.login.LoginPresenter
import es.bcg.tfm.migraine.presentation.features.login.LoginViewInteractor
import es.bcg.tfm.migraine.presentation.features.login.SignupPresenter
import es.bcg.tfm.migraine.presentation.features.signup.SignupViewInteractor
import es.bcg.tfm.migraine.ui.login.LoginContractView
import es.bcg.tfm.migraine.ui.login.viewmodel.UserViewModel

class SignupViewInteractorImpl(
    private val view: SignupContractView,
    private val presenter: SignupPresenter
) : SignupViewInteractor {

    override fun initialize() {
    }

    override fun init() {
        presenter.attachInteractor(this)
        presenter.initialize()
    }


    override fun destroy() {
        presenter.destroy()
    }

    override fun showError(error: FailureType) {
    }

    override fun logOut() {
    }

    override fun signupUser(userId: String, password: String, role: String?) {
        presenter.signupUser(userId, password, role)
    }

    override fun showSignupUserSuccess(signupModel: SignupModel) {
        view.showSignupOk()
    }

    override fun showSignupUserError(error: FailureType) {
        view.showSignupError()
    }

}