package es.bcg.tfm.migraine.ui.patientnavigation

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import es.bcg.tfm.migraine.R
import es.bcg.tfm.migraine.ui.patientnavigation.fragment.migraines.MigrainesFragment
import es.bcg.tfm.migraine.ui.patientnavigation.fragment.patientdetail.PatientDetailFragment
import es.bcg.tfm.migraine.ui.patientnavigation.fragment.patientsummary.PatientSummaryFragment

class PatientNavigationActivity : AppCompatActivity() {

    private var fragments = ArrayList<Fragment>(3)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(es.bcg.tfm.migraine.R.layout.activity_patient_info)

        val bottomNavigation: BottomNavigationView = findViewById(es.bcg.tfm.migraine.R.id.navigationView)

        bottomNavigation.setOnNavigationItemSelectedListener(
            object : BottomNavigationView.OnNavigationItemSelectedListener {
                override fun onNavigationItemSelected(item: MenuItem): Boolean {
                    when (item.itemId) {
                        R.id.patient_navigation_detail -> {
                            switchFragment(0)
                            return true
                        }
                        R.id.patient_navigation_summary -> {
                            switchFragment(1)
                            return true
                        }
                        R.id.patient_navigation_migraines -> {
                            switchFragment(2)
                            return true
                        }
                    }
                    return false
                }
            })

        fragments.add(PatientDetailFragment())
        fragments.add(PatientSummaryFragment())
        fragments.add(MigrainesFragment())

        switchFragment(1)
    }

    private fun switchFragment(pos: Int) {
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.frame_patient_navigation, fragments[pos])
            .commit()
    }

}