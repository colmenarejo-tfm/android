package es.bcg.tfm.migraine.ui.patientnavigation.fragment.patientdetail

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import es.bcg.tfm.migraine.R
// import es.bcg.tfm.migraine.core.executor.AndroidExecutor
import es.bcg.tfm.migraine.presentation.features.patients.PatientViewInteractor
import es.bcg.tfm.migraine.ui.patientnavigation.fragment.patientdetail.viewmodel.SociodemoPatientViewModel
import es.bcg.tfm.migraine.ui.studiesnavigation.fragmet.patients.viewmodel.PatientViewModel
import kotlinx.android.synthetic.main.activity_patient.*
import kotlinx.android.synthetic.main.item_patient.*
import java.math.BigDecimal

class PatientDetailFragment : Fragment(), PatientContractView {

    private val YELLOW_R: Int = 204
    private val YELLOW_G: Int = 153
    private val YELLOW_B: Int = 0

    private var patientsViewInteractor: PatientViewInteractor? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.activity_patient, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val patient: SociodemoPatientViewModel = SociodemoPatientViewModel(
            "vanesaclv@gmail.com",
            "Vanesa Calvo Broncano",
            "+34676251111",
            "M",
            "1986/04/06",
            "2019/05/05",
            "2020/05/05",
            "2019/01/18",
            14,
            "Universitarios",
            "Activo",
            "Migraña con aura",
            "Episódica (1–8 ep/mes)",
            true,
            12,
            false,
            false, false,
            true,
            "Fines de semana",
            "Mixta",
            true,
            true,
            true,
            false,
            false,
            "Ninguna",
             BigDecimal(21),
            64
        )


        if (patient.sex == "H") {
            patient_icon_sex.setImageResource(R.drawable.ic_male)
        } else {
            patient_icon_sex.setImageResource(R.drawable.ic_female)
        }

        patient_name.text = patient.name
//        if (patient.name == null) {
//            patient_edit.visibility = View.VISIBLE
//            // val patientPresenter = PatientDependency(AndroidExecutor(), MigraineApplication.localData).providePresenter()
//            // patientsViewInteractor = PatientViewInteractorImpl(this, patientPresenter)
//            patientsViewInteractor?.init()
//            patient_edit.setOnClickListener { PersonalDataDialog(
//                patientsViewInteractor!!, patient.uuid!!
//            ).show(getFragmentManager()!!, "PersonalDataDialog") }
//            patient_name.text = patient.pacienteEmail
//        } else {
//            if (patient.secondSurname != null) {
//                patient_name.text = "${patient.name} ${patient.surname} ${patient.secondSurname} "
//            } else {
//                patient_name.text = "${patient.name} ${patient.surname}"
//            }
//        }

        patient_date.text = patient.email
        patient_init_date.text = patient.startDate

        ptt_studies.text = patient.studyLevel
        ptt_employment_situation.text = patient.employment
        ptt_init_age.text = "${patient.startAge}"

        ptt_imc.text = "${patient.imc}"
        ptt_hit.text = "${patient.hit}"
        ptt_cognitive_reserve.text = "${patient.cognitiveReserve}"

        ptt_diagnosis.text = patient.diagnosis
        ptt_frequency_crisis.text = patient.crisisFrequency

        ptt_alcohol.text = patient.alcohol
        ptt_quality.text = patient.painQuality

        ptt_sleep_disturbance.text = patient.sleepDisturbance

        hasFeature(patient.background, ptt_background)
        hasFeature(patient.hypertension, ptt_hypertension)
        hasFeature(patient.diabetes, ptt_diabetes)
        hasFeature(patient.dyslipidemia, ptt_displemia)
        hasFeature(patient.smoker, ptt_smoker)
        hasFeature(patient.coffe, ptt_coffee)
        hasFeature(patient.stress, ptt_prolonged_stress)
        hasFeature(patient.anxiety, ptt_anxiety)
        hasFeature(patient.depression, ptt_depression)
        hasFeature(patient.menstruation, ptt_menstruation)

    }

    private fun hasFeature(feature: Boolean?, view: TextView) {
        if (feature != null && feature) {
            view.setTextColor(Color.rgb(YELLOW_R, YELLOW_G, YELLOW_B))
        }
    }

    override fun showPatientError() {
        patient_edit.visibility = View.GONE
        // TODO update screen
    }

    override fun showPatientOk(patientViewModel: PatientViewModel) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

}
