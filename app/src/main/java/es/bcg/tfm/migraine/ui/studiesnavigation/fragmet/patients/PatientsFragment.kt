package es.bcg.tfm.migraine.ui.studiesnavigation.fragmet.patients

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import es.bcg.tfm.migraine.MigraineApplication
import es.bcg.tfm.migraine.R
import es.bcg.tfm.migraine.core.executor.AndroidExecutor
import es.bcg.tfm.migraine.presentation.features.patients.PatientsDependency
import es.bcg.tfm.migraine.presentation.features.patients.PatientsViewInteractor
import es.bcg.tfm.migraine.ui.patientnavigation.PatientNavigationActivity
import es.bcg.tfm.migraine.ui.signup.SignupActivity
import es.bcg.tfm.migraine.ui.studiesnavigation.fragmet.patients.viewmodel.MinimalPatientViewModel
import es.bcg.tfm.migraine.ui.studiesnavigation.fragmet.patients.viewmodel.PatientsViewModel
import kotlinx.android.synthetic.main.activity_patients.*

class PatientsFragment : Fragment(), PatientsContractView {

    private var patientsViewInteractor: PatientsViewInteractor? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.activity_patients, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // TODO remove mock
        val list : MutableList<MinimalPatientViewModel> = mutableListOf()

        list.add(MinimalPatientViewModel("MG0319029", "vanesaclv@gmail.com", "Vanesa Calvo Broncano", "2017/05/05","M"))
        list.add(MinimalPatientViewModel("MG0319030", "sanmarher@gmail.com", "Santiago Martínez", "2018/29/06","H"))
        list.add(MinimalPatientViewModel("MG0419031", "cif1065@gmail.com", "Carmen Iglesias Ferreira", "2018/17/07","M"))
        list.add(MinimalPatientViewModel("MG0319029", "pepinho@gmail.com", "Pepe Merca Tuercas", "2018/02/04","H"))
        list.add(MinimalPatientViewModel("MG0319030", "bcg@gmail.com", "Borja Colmenarejo García", "2018/04/02","H"))
        list.add(MinimalPatientViewModel("MG0419031", "cevazquez@gmail.com", "Carolina Vázquez Caballero", "2018/02/03","M"))
        list.add(MinimalPatientViewModel("MG0319029", "sarpp01@gmail.com", "Sara Perez Perez", "2019/05/10","M"))
        list.add(MinimalPatientViewModel("MG0319030", "calvache13@gmail.com", "Virginia Calvache Ossuna", "2018/12/12","M"))
        list.add(MinimalPatientViewModel("MG0419031", "cif1065@gmail.com", "Jesús Navas Heras", "2018/03/24","H"))
        list.add(MinimalPatientViewModel("MG0319029", "vanesaclv@gmail.com", "Vanesa Calvo Broncano", "2018/09/11","M"))
        list.add(MinimalPatientViewModel("MG0319030", "sanmarher@gmail.com", "Santiago Martínez", "2018/05/05","H"))
        list.add(MinimalPatientViewModel("MG0419031", "cif1065@gmail.com", "Carmen Iglesias Ferreira", "2019/02/05","M"))

        list_patients.adapter = PatientArrayAdapter(context!!, list)

        list_patients.onItemClickListener = AdapterView.OnItemClickListener()
        { parent, view, position, id ->
            startActivity(Intent(context, PatientNavigationActivity::class.java))
        }

        // TODO uncomment
//        val patientsPresenter = PatientsDependency(AndroidExecutor(), MigraineApplication.localData).providePresenter()
//        patientsViewInteractor = PatientsViewInteractorImpl(this, patientsPresenter)
//        patientsViewInteractor?.init()
//
//        patientsViewInteractor?.patients()
    }

    override fun showPatientsOk(patientsViewModel: PatientsViewModel) {
        // TODO uncomment
//        val list = patientsViewModel.patients
//        list_patients.adapter = PatientArrayAdapter(context!!, list)
//        list_patients.onItemClickListener = AdapterView.OnItemClickListener()
//        { parent, view, position, id ->
//            val intent = Intent(context, PatientDetailFragment::class.java)
//            var bundle = Bundle()
//            bundle.putParcelable("patient", list.get(position))
//            intent.putExtra("bundlePatient",bundle)
//            startActivity(intent)
//        }
    }

    override fun showPatientsError() {
    }


    private inner class PatientArrayAdapter : ArrayAdapter<MinimalPatientViewModel> {

        internal var items: List<MinimalPatientViewModel>

        constructor(context: Context, objects: List<MinimalPatientViewModel>)
                : super(context, R.layout.item_patient, objects) {
            this.items = objects
        }

        private inner class ViewHolder(
            var sex: ImageView? = null,
            var name: TextView? = null,
            var email: TextView? = null,
            var phone : TextView? = null
        )

        override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
            var view: View?
            if (convertView == null) {
                view = layoutInflater.inflate(R.layout.item_patient, null)
                val viewHolder = ViewHolder()
                viewHolder.sex = view!!.findViewById(R.id.patient_icon_sex) as ImageView
                viewHolder.name = view.findViewById(R.id.patient_name) as TextView
                viewHolder.email = view.findViewById(R.id.patient_date) as TextView
                viewHolder.phone = view.findViewById(R.id.patient_init_date) as TextView
                view.tag = viewHolder
            } else {
                view = convertView
                (view.tag as ViewHolder)
            }
            val holder = view.tag as ViewHolder
            if (items[position].sex.equals("H")) {
                holder.sex!!.setImageResource(R.drawable.ic_male)
            } else {
                holder.sex!!.setImageResource(R.drawable.ic_female)
            }
            holder.email!!.text = items[position].email
            holder.phone!!.text = items[position].startDay
            holder.name!!.text = items[position].name

            if (holder.name!!.text == "Sara Perez Perez") {
                // TODO extract color
                view.setBackgroundColor(Color.rgb(204, 204, 255))
            } else {
                // TODO extract color
                view.setBackgroundColor(Color.rgb(240, 240, 245))
            }

//            if (items[position].name == null) {
//                holder.name!!.text = items[position].email
//                // TODO extract color
//                view.setBackgroundColor(Color.rgb(204, 204, 255))
//            } else {
//                holder.name!!.text = items[position].name
//                // TODO extract color
//                view.setBackgroundColor(Color.rgb(240, 240, 245))
//            }
            return view
        }

    }

}
