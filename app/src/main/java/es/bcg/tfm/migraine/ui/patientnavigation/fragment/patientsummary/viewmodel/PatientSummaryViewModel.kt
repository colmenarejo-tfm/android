package es.bcg.tfm.migraine.ui.patientnavigation.fragment.patientsummary.viewmodel

data class PatientSummaryViewModel (

    val total: String,

    val toPredict: String,

    val falses: String,

    val notMonitorized: String,

    val withNoise: String,

    val basalDays: String
)