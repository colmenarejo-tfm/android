package es.bcg.tfm.migraine.ui.patientnavigation.fragment.migraines

import android.graphics.Color
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import es.bcg.tfm.migraine.R
import es.bcg.tfm.migraine.ui.patientnavigation.fragment.migraines.viewmodel.MigraineViewModel
import kotlinx.android.synthetic.main.activity_patient_migraine_detail.*
import kotlinx.android.synthetic.main.item_migraine.*


class MigraineDetailActivity : AppCompatActivity() {

    private val GOOD_R : Int = 149
    private val GOOD_G : Int = 221
    private val GOOD_B : Int = 0

    private val FALSE_R : Int = 255
    private val FALSE_G : Int = 153
    private val FALSE_B : Int = 0

    private val BAD_R : Int = 149
    private val BAD_G : Int = 0
    private val BAD_B : Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_patient_migraine_detail)

        val migraine = intent.getBundleExtra("bundleMigraine")
            .getParcelable<MigraineViewModel>("migraine") as MigraineViewModel

        migraine_id.text = "Migraine ${migraine.id}"

        if (migraine.status == "GOOD") {
            migraine_icon.setColorFilter(Color.rgb(GOOD_R, GOOD_G, GOOD_B))
        } else if (migraine.status == "FALSE"){
            migraine_icon.setColorFilter(Color.rgb(FALSE_R, FALSE_G, FALSE_B))
        } else {
            migraine_icon.setColorFilter(Color.rgb(BAD_R, BAD_G, BAD_B))
        }

        migraine_init_date.text = migraine.initDate
        migraine_end_date.text = migraine.endDate

        pmd_cure.text = migraine.cure
        pmd_pain_intensity.text = "${migraine.painIntensity}"
        pmd_pain_evol.text = "${migraine.painEvol}"
        pmd_premonitory.text = migraine.premonitory
        pmd_medicines.text = migraine.medicines
        pmd_symptom.text = migraine.symptom
    }

}
