package es.bcg.tfm.migraine.ui.login

import es.bcg.tfm.migraine.ui.login.viewmodel.UserViewModel

interface LoginContractView {

    fun showLoginOk(userViewModel: UserViewModel)

    fun showLoginError()

}