package es.bcg.tfm.migraine.ui.patientnavigation.fragment.patientdetail.viewmodel

import java.math.BigDecimal

data class SociodemoPatientViewModel (
    var email: String,
    var name: String,
    var phone: String? = null,
    var sex: String,
    var birthdayDate: String,
    var startDate: String,
    val endDate: String? = null,
    val studyStartDate: String,
    val cognitiveReserve: Int,
    val studyLevel: String,
    val employment: String,
    val diagnosis: String,
    val crisisFrequency: String,
    val background: Boolean,
    val startAge: Int,
    val hypertension: Boolean,
    val diabetes: Boolean,
    val dyslipidemia: Boolean,
    val smoker: Boolean,
    val alcohol: String,
    val painQuality: String,
    val menstruation: Boolean,
    val coffe: Boolean,
    val stress: Boolean,
    val anxiety: Boolean,
    val depression: Boolean,
    val sleepDisturbance: String,
    val imc: BigDecimal,
    val hit: Int
)