package es.bcg.tfm.migraine.ui.studiesnavigation.fragmet.patients

import es.bcg.tfm.migraine.domain.core.exception.FailureType
import es.bcg.tfm.migraine.domain.model.patient.PatientsModel
import es.bcg.tfm.migraine.presentation.features.patients.PatientsPresenter
import es.bcg.tfm.migraine.presentation.features.patients.PatientsViewInteractor
import es.bcg.tfm.migraine.ui.studiesnavigation.fragmet.patients.viewmodel.PatientViewModel
import es.bcg.tfm.migraine.ui.studiesnavigation.fragmet.patients.viewmodel.PatientsViewModel

class PatientsViewInteractorImpl(
    private val view: PatientsContractView,
    private val presenter: PatientsPresenter
) : PatientsViewInteractor {

    override fun initialize() {
    }

    override fun init() {
        presenter.attachInteractor(this)
        presenter.initialize()
    }


    override fun destroy() {
        presenter.destroy()
    }

    override fun showError(error: FailureType) {}

    override fun logOut() {}

    override fun patients() {
        presenter.patients()
    }

    override fun showPatientsSuccess(patientsModel: PatientsModel) {
        view.showPatientsOk(PatientsViewModel.transform(patientsModel))
    }

    override fun showPatientsError(error: FailureType) {
        view.showPatientsError()
    }

}