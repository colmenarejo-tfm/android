package es.bcg.tfm.migraine.ui.studiesnavigation.fragmet.summary.viewmodel

data class SummaryViewModel(

    val month: String,

    val weeks: Map<Int, Int>, // Week - active patients

    val basalExpected: Int,

    val migrainesExpected: Int,

    val patients: List<PatientSummary>

)

data class PatientSummary(

    val name: String,

    val weeks: Map<Int, Int> // Week - active (0 active  or 1 inactive)

)