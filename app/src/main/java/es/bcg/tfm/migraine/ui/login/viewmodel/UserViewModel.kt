package es.bcg.tfm.migraine.ui.login.viewmodel

import es.bcg.tfm.migraine.domain.model.user.UserModel

data class UserViewModel(
    val token: String
) {

    companion object {
        fun transform(userModel: UserModel) =
                UserViewModel(userModel.token)
    }
}