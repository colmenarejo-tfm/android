package es.bcg.tfm.migraine.ui.studiesnavigation.fragmet.patients

import es.bcg.tfm.migraine.ui.studiesnavigation.fragmet.patients.viewmodel.PatientsViewModel

interface PatientsContractView {

    fun showPatientsOk(patientsViewModel: PatientsViewModel)

    fun showPatientsError()

}
