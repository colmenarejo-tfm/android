package es.bcg.tfm.migraine.ui.patientnavigation.fragment.patientdetail

import es.bcg.tfm.migraine.domain.core.exception.FailureType
import es.bcg.tfm.migraine.domain.model.patient.PatientModel
import es.bcg.tfm.migraine.presentation.features.patients.PatientPresenter
import es.bcg.tfm.migraine.presentation.features.patients.PatientViewInteractor
import es.bcg.tfm.migraine.ui.studiesnavigation.fragmet.patients.viewmodel.PatientViewModel

class PatientViewInteractorImpl(
    private val view: PatientContractView,
    private val presenter: PatientPresenter
) : PatientViewInteractor {

    override fun initialize() {
    }

    override fun init() {
        presenter.attachInteractor(this)
        presenter.initialize()
    }


    override fun destroy() {
        presenter.destroy()
    }

    override fun showError(error: FailureType) {}

    override fun logOut() {}

    override fun personalInfo(
        uuid: String,
        name: String,
        surname: String,
        secondSurname: String?,
        phone: String,
        comment: String?
    ) {
        presenter.personalInfo(uuid, name, surname, secondSurname, phone, comment)
    }

    override fun showPatientSuccess(patientModel: PatientModel) {
        view.showPatientOk(PatientViewModel.transform(patientModel))
    }

    override fun showPatientsError(error: FailureType) {
        view.showPatientError()
    }

}