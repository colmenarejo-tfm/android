package es.bcg.tfm.migraine.ui.studiesnavigation.fragmet.patients.viewmodel

import es.bcg.tfm.migraine.domain.model.patient.PatientsModel

data class PatientsViewModel(

    val patients: List<PatientViewModel>

) {

    companion object {
        fun transform(patientsModel: PatientsModel) =
            PatientsViewModel(patientsModel.patients.map { patient ->
                PatientViewModel(
                    patient.uuid,
                    patient.pacienteEmail,
                    patient.inicioDelEstudio,
                    patient.nacimiento,
                    patient.sexo,
                    patient.reservaCognitiva,
                    patient.estudios,
                    patient.situacionLaboral,
                    patient.diagnostico,
                    patient.frecuenciaCrisis,
                    patient.antecedentes,
                    patient.edadInicio,
                    patient.hipertension,
                    patient.diabetes,
                    patient.displemia,
                    patient.fumador,
                    patient.alcohol,
                    patient.cualidad,
                    patient.menstruacion,
                    patient.cafe,
                    patient.estresProlongado,
                    patient.ansiedad,
                    patient.depresion,
                    patient.alteracionSuenyo,
                    patient.imc,
                    patient.hit,
                    patient.ttoGeneral,
                    patient.ttoPreventivo,
                    patient.name,
                    patient.surname,
                    patient.secondSurname,
                    patient.phone,
                    patient.comment
                )
            })
    }
}