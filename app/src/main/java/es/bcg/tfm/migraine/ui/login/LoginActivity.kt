package es.bcg.tfm.migraine.ui.login

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import es.bcg.tfm.migraine.R
// import es.bcg.tfm.migraine.core.executor.AndroidExecutor
import es.bcg.tfm.migraine.presentation.features.login.LoginViewInteractor
import es.bcg.tfm.migraine.ui.login.viewmodel.UserViewModel
import es.bcg.tfm.migraine.ui.patientnavigation.PatientNavigationActivity
import es.bcg.tfm.migraine.ui.studiesnavigation.fragmet.patients.PatientsFragment
import es.bcg.tfm.migraine.ui.patientnavigation.fragment.migraines.MigrainesFragment
import es.bcg.tfm.migraine.ui.patientnavigation.fragment.patientsummary.PatientSummaryFragment
import es.bcg.tfm.migraine.ui.signup.SignupActivity
import es.bcg.tfm.migraine.ui.studies.StudiesActivity
import kotlinx.android.synthetic.main.activity_login.*


/**
 * A login screen that offers login via email/password.
 */
class LoginActivity : AppCompatActivity(), LoginContractView {

    private var loginViewInteractor: LoginViewInteractor? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        btn_login.setOnClickListener { login() }
        link_new_user.setOnClickListener { register() }
        link_forgot_password.setOnClickListener { rememberPassword() }

        // val loginPresenter = LoginDependency(AndroidExecutor(), MigraineApplication.localData).providePresenter()
        // loginViewInteractor = LoginViewInteractorImpl(this, loginPresenter)
        loginViewInteractor?.init()
    }

    override fun onDestroy() {
        super.onDestroy()
        loginViewInteractor?.destroy()
    }


    private fun register() {
        startActivity(Intent(baseContext, SignupActivity::class.java))
    }

    private fun rememberPassword() {
        ForgotPasswordDialog().show(supportFragmentManager, "ForgotPasswordDialog")
    }

    // Action login
    private fun login() {
        startActivity(Intent(this, StudiesActivity::class.java))
        // TODO uncomment
//        if (email.text != null && password.text != null) {
//            loginViewInteractor?.loginUser("${email.text}" ,"${password.text}")
//            btn_login.isEnabled = false
//            link_new_user.isEnabled = false
//            link_forgot_password.isEnabled = false
//            email.isEnabled = false
//            password.isEnabled = false
//        }
    }

    override fun showLoginOk(userViewModel: UserViewModel) {
        startActivity(Intent(baseContext, StudiesActivity::class.java))
        enableScreen()
    }

    override fun showLoginError() {
        enableScreen()
    }

    private fun enableScreen(){
        btn_login.isEnabled = true
        link_new_user.isEnabled = true
        link_forgot_password.isEnabled = true
        email.isEnabled = true
        password.isEnabled = true
    }
}
