package es.bcg.tfm.migraine.ui.signup

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import es.bcg.tfm.migraine.MigraineApplication
import es.bcg.tfm.migraine.R
// import es.bcg.tfm.migraine.core.executor.AndroidExecutor
import es.bcg.tfm.migraine.presentation.features.signup.SignupDependency
import es.bcg.tfm.migraine.presentation.features.signup.SignupViewInteractor
import es.bcg.tfm.migraine.ui.login.LoginActivity
import kotlinx.android.synthetic.main.activity_signup.*

class SignupActivity : AppCompatActivity(), SignupContractView {

    private var signupViewInteractor: SignupViewInteractor? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signup)
        btn_register.setOnClickListener { register() }
        link_login.setOnClickListener { login() }

        // val signupPresenter = SignupDependency(AndroidExecutor(), MigraineApplication.localData).providePresenter()
        // signupViewInteractor = SignupViewInteractorImpl(this, signupPresenter)
        signupViewInteractor?.init()
    }

    private fun login() {
        startActivity(Intent(this, LoginActivity::class.java))
    }

    private fun register() {
        if (email.text != null && password.text != null) {
            signupViewInteractor?.signupUser("${email.text}", "${password.text}")
            btn_register.isEnabled = false
            link_login.isEnabled = false
        }
    }

    override fun showSignupOk() {
        login()
    }

    override fun showSignupError() {
        btn_register.isEnabled = true
        link_login.isEnabled = true
    }

}
