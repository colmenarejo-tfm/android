package es.bcg.tfm.migraine.ui.studies.viewmodel

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class StudyViewModel(

    val id: Int,

    val name: String,

    val startDate: String

) : Parcelable
