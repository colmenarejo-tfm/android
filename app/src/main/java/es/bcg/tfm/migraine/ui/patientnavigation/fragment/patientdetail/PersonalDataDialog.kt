package es.bcg.tfm.migraine.ui.patientnavigation.fragment.patientdetail

import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import es.bcg.tfm.migraine.R
import es.bcg.tfm.migraine.presentation.features.patients.PatientViewInteractor
import kotlinx.android.synthetic.main.dialog_personal_data.*

class PersonalDataDialog(
    val patientViewInteractor: PatientViewInteractor,
    val uuid: String
) : DialogFragment() {

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return AlertDialog.Builder(activity!!)
            .setMessage(R.string.title_personal_data)
            .setView(activity!!.layoutInflater.inflate(R.layout.dialog_personal_data, null))
            .setPositiveButton(R.string.send, DialogInterface.OnClickListener { dialog, id ->
                val name = personal_data_name?.text

                patientViewInteractor.personalInfo(uuid, "${name}", "${personal_data_surname?.text}",
                    "${personal_data_second_surname?.text}",
                    "${personal_data_phone?.text}", "${personal_data_comment?.text}")
            }).setNegativeButton(R.string.cancel, DialogInterface.OnClickListener { dialog, id ->
                this.dialog?.cancel()
            }).create()
    }
}