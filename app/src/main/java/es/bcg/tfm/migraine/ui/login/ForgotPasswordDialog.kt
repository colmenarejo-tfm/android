package es.bcg.tfm.migraine.ui.login

import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import es.bcg.tfm.migraine.R

class ForgotPasswordDialog : DialogFragment() {

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return AlertDialog.Builder(activity!!)
            .setMessage(R.string.forgot_password_title)
            .setView(activity!!.layoutInflater.inflate(R.layout.dialog_forgot_password, null))
            .setPositiveButton(R.string.send, DialogInterface.OnClickListener { dialog, id ->
                // TODO remember password logic
            }).setNegativeButton(R.string.cancel, DialogInterface.OnClickListener { dialog, id ->
                this.dialog?.cancel()
            }).create()
    }

}