package es.bcg.tfm.migraine.ui.studiesnavigation.fragmet.patients.viewmodel

import android.os.Parcelable
import es.bcg.tfm.migraine.domain.model.patient.PatientModel
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PatientViewModel(

    val uuid: String?,

    val pacienteEmail: String?,

    val inicioDelEstudio: String?,

    val nacimiento: String?,

    val sexo: String?,

    val reservaCognitiva: Int?,

    val estudios: String?,

    val situacionLaboral: String?,

    val diagnostico: String?,

    val frecuenciaCrisis: String?,

    val antecedentes: Boolean?,

    val edadInicio: Int?,

    val hipertension: Boolean?,

    val diabetes: Boolean?,

    val displemia: Boolean?,

    val fumador: Boolean?,

    val alcohol: String?,

    val cualidad: String?,

    val menstruacion: Boolean?,

    val cafe: Boolean?,

    val estresProlongado: Boolean?,

    val ansiedad: Boolean?,

    val depresion: Boolean?,

    val alteracionSuenyo: String?,

    val imc: Double?,

    val hit: Int?,

    val ttoGeneral: String?,

    val ttoPreventivo: String?,

    val name: String? = null,

    val surname: String? = null,

    val secondSurname: String? = null,

    val phone: String? = null,

    val comment: String? = null

) : Parcelable {

    companion object {
        fun transform(patient: PatientModel) =
            PatientViewModel(
                patient.uuid,
                patient.pacienteEmail,
                patient.inicioDelEstudio,
                patient.nacimiento,
                patient.sexo,
                patient.reservaCognitiva,
                patient.estudios,
                patient.situacionLaboral,
                patient.diagnostico,
                patient.frecuenciaCrisis,
                patient.antecedentes,
                patient.edadInicio,
                patient.hipertension,
                patient.diabetes,
                patient.displemia,
                patient.fumador,
                patient.alcohol,
                patient.cualidad,
                patient.menstruacion,
                patient.cafe,
                patient.estresProlongado,
                patient.ansiedad,
                patient.depresion,
                patient.alteracionSuenyo,
                patient.imc,
                patient.hit,
                patient.ttoGeneral,
                patient.ttoPreventivo,
                patient.name,
                patient.surname,
                patient.secondSurname,
                patient.phone,
                patient.comment
            )
    }
}